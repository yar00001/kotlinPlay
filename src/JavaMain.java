public class JavaMain {

    public static void javaStaticMethod() {
        final int a = 1;
        final int b = 2;
        System.out.println(String.format("The abs value is: %d", Math.abs(b - a)));
    }

    public static void main(String[] args) {
        System.out.println("this is a test");
        BasicsKt.printSumVoid(2,3);
    }
}
