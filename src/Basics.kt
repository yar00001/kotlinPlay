
// FUNCTIONS
////////////////////////////////
// http://kotlinlang.org/docs/reference/basic-syntax.html#defining-functions
fun sum(a: Int, b: Int): Int {
    return a + b
}

fun divide(a: Int, b: Int) = a/b

fun printSumVoidUnit(a:Int, b:Int) : Unit {
    println("sum is ${a+b}")
}

fun printSumVoid(a:Int, b:Int) {
    println("sum is ${a+b}")
}

// VARIABLES
////////////////////////////////
fun variableTypes() {

    // local vars - IMMUTABLE (read-only)
    // i.e Constants
    val a: Int = 1
    val b = 2      // Int is inferred
    val c: Int
    c = 3

    // MUTABLE VARS
    var x = 5
    x += 1
}

fun stringTemplate() {
    var a = 1
    var s1 = "a is $a";

    a = 2
    val s2 = "${s1.replace("is", "was")}, but is $a"

    println(s2)
}

// NULLABLE
////////////////////////////////
fun parseIntNullable(str: String) : Int? {
    // Int can be null!
    return str.toIntOrNull()
}

// CASTING
////////////////////////////////
fun objCastParsing(obj: Any): Int? {
    if (obj is String) {
        // after using 'is' the casting is being done automatically
        return obj.length
    }
    if (obj is Int) {
        return obj - 1;
    }
    return null
}

// LOOP
////////////////////////////////


fun main(args: Array<String>) {
    // type 1
    println("sum of 1 and 2: " + sum(1,2))
    // type 2
    println("div test: ${divide(4,2)}")
    // type 3 - void
    printSumVoidUnit(3,4)
    // type 4 - void
    printSumVoid(3,4)

    // string template
    stringTemplate()

    // Nullable test
    val x = parseIntNullable("test")
    val y = parseIntNullable("12")
    if (x == null) println("x is null")
    if (y != null) println("y is not null")

    // AutoCasting
    println("objCastTesting" + objCastParsing("testing"))
    println("objCastTesting" + objCastParsing(12))
}
